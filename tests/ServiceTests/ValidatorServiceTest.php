<?php

declare(strict_types=1);

namespace App\Tests;

use App\Service\ValidatorService;
use PHPUnit\Framework\TestCase;

class ValidatorServiceTest extends TestCase
{
    const TEST_PRODUCTS = [
        [
            'Product Code' => 'P0002',
            'Product Name' => 'Cd Player',
            'Product Description' => 'Nice CD player1',
            'Stock' => '1001',
            'Cost in GBP' => '12',
            'Discontinued' => 'yes',
        ],
        [
            'Product Code' => 'P0030',
            'Product Name' => '',
            'Product Description' => '',
            'Stock' => '',
            'Cost in GBP' => '1001',
            'Discontinued' => '',
        ],
        [
            'Product Code' => '',
            'Product Name' => 'valid name',
            'Product Description' => 'normal desc',
            'Stock' => '',
            'Cost in GBP' => '900',
            'Discontinued' => 'true',
        ],
    ];

    const ERROR_LIST = [
        null,
        [
            'P0030 has invalid Name',
            'P0030 has invalid Description',
            'P0030 has invalid Stock',
            'P0030 has Price more then max',
        ],
        [
            ' has invalid Code',
            ' has invalid Stock',
        ],
    ];

    public function testIsValid()
    {
        $validator = new ValidatorService(self::TEST_PRODUCTS[0]);

        self::assertTrue($validator->isValid());
    }

    public function testGetErrors()
    {
        for ($i = 1; $i < count(self::TEST_PRODUCTS); ++$i) {
            $validator = new ValidatorService(self::TEST_PRODUCTS[$i]);
            $validator->isValid();

            self::assertEquals(self::ERROR_LIST[$i], $validator->getErrors());
        }
    }
}
