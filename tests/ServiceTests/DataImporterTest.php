<?php

declare(strict_types=1);

namespace App\Tests;

use App\Entity\Product;
use App\Repository\ProductRepository;
use App\Service\DataImporter;
use Doctrine\ORM\NonUniqueResultException;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Port\Doctrine\DoctrineWriter;

class DataImporterTest extends TestCase
{
    /**
     * @var DataImporter
     */
    private $dataImporter;

    const MODEL_RESULTS = [
        [
            'title' => 'total',
            'value' => 3,
        ],
        [
            'title' => 'skipped',
            'value' => 1,
        ],
        [
            'title' => 'imported',
            'value' => 1,
        ],
        [
            'title' => 'updated',
            'value' => 1,
        ],
    ];

    protected function setUp()
    {
        $this->dataImporter = new DataImporter($this->getRepositoryMock());
        $this->dataImporter->setDoctrineWriter($this->getDoctrineWriterMock());
    }

    /**
     * @return MockObject
     */
    protected function getRepositoryMock(): MockObject
    {
        $repositoryMock = $this->createMock(ProductRepository::class);
        $repositoryMock->expects(self::at(0))
            ->method('findByCode')
            ->with('P0001')
            ->willReturn(null);
        $repositoryMock->expects(self::at(1))
            ->method('findByCode')
            ->with('P0003')
            ->willReturn(new Product('P0003', 'VCR', 'Top notch VCR', 12, 333, true));

        return $repositoryMock;
    }

    /**
     * @return MockObject
     */
    private function getDoctrineWriterMock(): MockObject
    {
        return $this->createMock(DoctrineWriter::class);
    }

    /**
     * @throws NonUniqueResultException
     */
    public function testGetResults()
    {
        $this->dataImporter->processFile('tests/stock.csv');

        self::assertEquals(self::MODEL_RESULTS, $this->dataImporter->getResults());
    }

    /**
     * @throws NonUniqueResultException
     */
    public function testGetErrors()
    {
        $this->dataImporter->processFile('tests/stock.csv');

        self::assertEquals([['P0002 has Price more then max']], $this->dataImporter->getErrors());
    }
}
