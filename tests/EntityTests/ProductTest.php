<?php

declare(strict_types=1);

namespace App\Tests\EntityTests;

use App\Entity\Product;
use PHPUnit\Framework\TestCase;

class ProductTest extends TestCase
{
    const PRODUCT_DATA = [
        'Product Code' => 'P0001',
        'Product Name' => 'TV',
        'Product Description' => '32 Tv',
        'Stock' => 10,
        'Cost in GBP' => 400,
        'Discontinued' => false,
    ];

    const UPDATED_PRODUCT = [
        'Product Code' => 'P0002',
        'Product Name' => 'TV XS',
        'Product Description' => '32 Tv',
        'Stock' => 25,
        'Cost in GBP' => 1200,
        'Discontinued' => false,
    ];

    /**
     * @var Product
     */
    private $product;

    protected function setUp()
    {
        $this->product = new Product(self::UPDATED_PRODUCT['Product Code']);
    }

    public function testSettersAndGetters()
    {
        $this->product->setCode(self::PRODUCT_DATA['Product Code']);
        $this->product->setName(self::PRODUCT_DATA['Product Name']);
        $this->product->setDescription(self::PRODUCT_DATA['Product Description']);
        $this->product->setStock(self::PRODUCT_DATA['Stock']);
        $this->product->setPrice(self::PRODUCT_DATA['Cost in GBP']);
        $this->product->setDiscontinued(self::PRODUCT_DATA['Discontinued']);

        self::assertEquals(self::PRODUCT_DATA['Product Code'], $this->product->getCode());
        self::assertEquals(self::PRODUCT_DATA['Product Name'], $this->product->getName());
        self::assertEquals(self::PRODUCT_DATA['Product Description'], $this->product->getDescription());
        self::assertEquals(self::PRODUCT_DATA['Stock'], $this->product->getStock());
        self::assertEquals(self::PRODUCT_DATA['Cost in GBP'], $this->product->getPrice());
        self::assertEquals(self::PRODUCT_DATA['Discontinued'], $this->product->isDiscontinued());
    }

    public function testProductUpdate()
    {
        $this->product->update(
            self::UPDATED_PRODUCT['Product Name'],
            self::UPDATED_PRODUCT['Product Description'],
            self::UPDATED_PRODUCT['Cost in GBP'],
            self::UPDATED_PRODUCT['Stock'],
            self::UPDATED_PRODUCT['Discontinued']
        );

        self::assertEquals(self::UPDATED_PRODUCT['Product Code'], $this->product->getCode());
        self::assertEquals(self::UPDATED_PRODUCT['Product Name'], $this->product->getName());
        self::assertEquals(self::UPDATED_PRODUCT['Product Description'], $this->product->getDescription());
        self::assertEquals(self::UPDATED_PRODUCT['Stock'], $this->product->getStock());
        self::assertEquals(self::UPDATED_PRODUCT['Cost in GBP'], $this->product->getPrice());
        self::assertEquals(self::UPDATED_PRODUCT['Discontinued'], $this->product->isDiscontinued());
    }
}
