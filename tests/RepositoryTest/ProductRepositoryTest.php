<?php

declare(strict_types=1);

namespace App\Tests\RepositoryTest;

use App\Entity\Product;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ProductRepositoryTest extends KernelTestCase
{
    const TEST_PRODUCTS = [
        [
            'Product Code' => 'P0001',
            'Product Name' => 'TV',
            'Product Description' => '32” Tv',
            'Stock' => 10,
            'Cost in GBP' => 999,
            'Discontinued' => false,
        ],
        [
            'Product Code' => 'P0002',
            'Product Name' => 'Cd Player',
            'Product Description' => 'Nice CD player',
            'Stock' => 11,
            'Cost in GBP' => 1001,
            'Discontinued' => true,
        ],
        [
            'Product Code' => 'P0003',
            'Product Name' => 'VCR',
            'Product Description' => 'Top notch VCR',
            'Stock' => 12,
            'Cost in GBP' => 39,
            'Discontinued' => true,
        ],
    ];

    /**
     * @var EntityManager
     */
    private $entityManager;

    protected function setUp()
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    public function testFindByCode()
    {
        for ($i = 0; $i < count(self::TEST_PRODUCTS); ++$i) {
            $products = $this->entityManager
                ->getRepository(Product::class)
                ->findByCode(self::TEST_PRODUCTS[$i]['Product Code']);

            $testProduct = $this->entityManager
                ->getRepository(Product::class)
                ->find(self::TEST_PRODUCTS[$i]['Product Code']);

            self::assertEquals(self::TEST_PRODUCTS[$i]['Product Code'], $products->getCode());
            self::assertEquals(self::TEST_PRODUCTS[$i]['Product Name'], $products->getName());
            self::assertEquals(self::TEST_PRODUCTS[$i]['Product Description'], $products->getDescription());
            self::assertEquals(self::TEST_PRODUCTS[$i]['Stock'], $products->getPrice());
            self::assertEquals(self::TEST_PRODUCTS[$i]['Cost in GBP'], $products->getStock());
            self::assertEquals(self::TEST_PRODUCTS[$i]['Discontinued'], $products->isDiscontinued());
            self::assertEquals($testProduct, $products);
        }
    }

    protected function tearDown()
    {
        parent::tearDown();

        $this->entityManager->close();
        $this->entityManager = null;
    }
}
