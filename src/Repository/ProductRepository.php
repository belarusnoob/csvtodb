<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Doctrine\ORM\QueryBuilder;

class ProductRepository extends ServiceEntityRepository
{
    /**
     * @var QueryBuilder
     */
    private $qb;

    /**
     * ProductRepository constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Product::class);
    }

    /**
     * @param $value
     *
     * @return Product|null
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findByCode($value): ?Product
    {
        $this->qb = $this->createQueryBuilder('p');

        return $this->qb
            ->setParameter('code', $value)
            ->where($this->qb->expr()->eq('p.code', ':code'))
            ->getQuery()
            ->getOneOrNullResult();
    }
}
