<?php

namespace App\DataFixtures;

use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $manager->persist(new Product('P0001', 'TV', '32” Tv', 10, 999, false));
        $manager->persist(new Product('P0002', 'Cd Player', 'Nice CD player', 11, 1001, 'yes'));
        $manager->persist(new Product('P0003', 'VCR', 'Top notch VCR', 12, 39.33, 'yes'));

        $manager->flush();
    }
}
