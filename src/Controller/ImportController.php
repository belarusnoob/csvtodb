<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Product;
use App\Form\UploadFormType;
use App\Repository\ProductRepository;
use App\Service\DataImporter;
use Doctrine\Common\Persistence\ObjectManager;
use Port\Doctrine\DoctrineWriter;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ImportController extends AbstractController
{
    /**
     * @var DataImporter
     */
    private $dataImporter;

    /**
     * ImportController constructor.
     *
     * @param RegistryInterface $doctrine
     */
    public function __construct(RegistryInterface $doctrine)
    {
        $this->configurateDataImporter($doctrine->getManagerForClass(Product::class));
    }

    /**
     * @param ObjectManager $em
     */
    private function configurateDataImporter(ObjectManager $em): void
    {
        $this->dataImporter = new DataImporter($this->getProductRepository($em));
        $this->dataImporter->setDoctrineWriter($this->getDoctrineWriter($em));
    }

    /**
     * @param ObjectManager $em
     *
     * @return DoctrineWriter|null
     */
    private function getDoctrineWriter(ObjectManager $em): ?DoctrineWriter
    {
        $doctrineWriter = new DoctrineWriter($em, Product::class, 'code');
        $doctrineWriter->disableTruncate()->prepare();

        return $doctrineWriter;
    }

    /**
     * @param ObjectManager $em
     *
     * @return ProductRepository|null
     */
    private function getProductRepository(ObjectManager $em): ?ProductRepository
    {
        return $em->getRepository(Product::class);
    }

    /**
     * @Route("/", name="index")
     *
     * @return Response
     */
    public function index(): Response
    {
        return $this->render('/import/index.html.twig', [
            'form' => $this->createForm(UploadFormType::class)->createView()
        ]);
    }

    /**
     * @Route("/doImport", name="import")
     *
     * @param Request $request
     *
     * @return Response
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function import(Request $request): Response
    {
        $form = $this->createForm(UploadFormType::class)->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $file = $form['csv_file']->getData()->getRealPath();
            $this->dataImporter->processFile($file);

            return $this->render('/import/index.html.twig', [
                'errors' => $this->dataImporter->getErrors(),
                'message' => 'Success',
                'info' => $this->dataImporter->getResults(),
                'form' => $form->createView(),
            ]);
        }

        return $this->render('/import/index.html.twig', [
            'message' => 'Invalid',
            'form' => $form->createView(),
        ]);
    }
}
