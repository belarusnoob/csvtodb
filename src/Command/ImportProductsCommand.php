<?php

declare(strict_types=1);

namespace App\Command;

use App\Service\DataImporter;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ImportProductsCommand extends command
{
    private $dataImporter;

    /**
     * ImportProductsCommand constructor.
     *
     * @param DataImporter $dataImporter
     */
    public function __construct(DataImporter $dataImporter)
    {
        parent::__construct();
        $this->dataImporter = $dataImporter;
    }

    protected function configure()
    {
        $this
            ->setName('products:import')
            ->setDescription('This command allows you import products list in DB.')
            ->addArgument('file', InputArgument::REQUIRED, 'Location of the CSV-file to read products')
            ->addArgument('test', InputArgument::OPTIONAL, 'Run command in test mode (without import)');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->dataImporter->processFile($input->getArgument('file'));
        $this->makeResult(
            $this->dataImporter->getErrors(),
            $this->dataImporter->getResults()
        );
    }

    /**
     * @param array $errors
     * @param array $results
     */
    public function makeResult(array $errors, array $results)
    {
        foreach ($results as $result) {
            foreach ($result as $value) {
                echo "|$value|";
            }
            echo "\n";
        }

        foreach ($errors as $error) {
            foreach ($error as $suberror) {
                echo "$suberror \n";
            }
        }
    }
}
