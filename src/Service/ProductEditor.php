<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Product;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

class ProductEditor
{
    const STATUS_CREATED = 1;
    const STATUS_UPDATED = 2;

    /**
     * @var RegistryInterface
     */
    private $doctrine;

    /**
     * @var ObjectManager|null
     */
    private $em;

    /**
     * @var ObjectRepository
     */
    private $productRepository;

    /**
     * ProductEditor constructor.
     *
     * @param RegistryInterface $doctrine
     */
    public function __construct(RegistryInterface $doctrine)
    {
        $this->doctrine = $doctrine;
        $this->em = $this->doctrine->getManagerForClass(Product::class);
        $this->productRepository = $this->em->getRepository(Product::class);
    }

    /**
     * @param array $item
     *
     * @return int
     *
     * @throws NonUniqueResultException
     */
    public function createOrEdit(array $item): int
    {
        return
            !$this->productRepository->findByCode($item['Product Code']) ?
            $this->createProduct($item) :
            $this->editProduct($this->productRepository->findByCode($item['Product Code']), $item);
    }

    /**
     * @param array $item
     *
     * @return int
     */
    private function createProduct(array $item): int
    {
        $product = new Product(
            $item['Product Code'],
            $item['Product Name'],
            $item['Product Description'],
            (int) $item['Cost in GBP'],
            (int) $item['Stock'],
            'yes' == $item['Discontinued'] ? true : false
        );

        $this->em->persist($product);

        return self::STATUS_CREATED;
    }

    /**
     * @param Product $product
     * @param array   $item
     *
     * @return int
     */
    private function editProduct(Product $product, array $item): int
    {
        $product->update(
            $item['Product Name'],
            $item['Product Description'],
            (int) $item['Cost in GBP'],
            (int) $item['Stock'],
            'yes' == $item['Discontinued'] ? true : false
        );

        return self::STATUS_UPDATED;
    }
}
