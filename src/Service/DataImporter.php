<?php

declare(strict_types=1);

namespace App\Service;

use App\Repository\ProductRepository;
use Port\Csv\CsvReader;
use Port\Doctrine\DoctrineWriter;
use Doctrine\Common\Persistence\ObjectRepository;

class DataImporter
{
    const BUTCH_SIZE = 200;

    /**
     * @var ObjectRepository
     */
    public $repository;

    /**
     * @var DoctrineWriter
     */
    public $doctrineWriter;

    /**
     * @var int
     */
    private $effected = 0;

    /**
     * @var int
     */
    private $affected = 0;

    /**
     * @var int
     */
    private $updated = 0;

    /**
     * @var array
     */
    private $errors = [];

    /**
     * DataImporter constructor.
     *
     * @param ProductRepository $repository
     */
    public function __construct(ProductRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param string $doctrineWriter
     */
    public function setDoctrineWriter($doctrineWriter): void
    {
        $this->doctrineWriter = $doctrineWriter;
    }

    /**
     * @param string $filename
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function processFile(string $filename): void
    {
        foreach ($this->getFileContent($filename) as $product) {
            $this->makeResult($product);
        }
        $this->doctrineWriter->finish();
    }

    /**
     * @param string filename
     *
     * @return CsvReader|null
     */
    private function getFileContent(string $filename): ?CsvReader
    {
        $csvReader = new CsvReader(new \SplFileObject($filename));
        $csvReader->setHeaderRowNumber(0);

        return $csvReader;
    }

    /**
     * @param array $product
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    private function makeResult(array $product): void
    {
        $validator = new ValidatorService($product);

        if (!$validator->isValid()) {
            ++$this->affected;
            $this->errors[] = $validator->getErrors();
        } else {
            $this->repository->findByCode($product['Product Code']) ? $this->updated++ : $this->effected++;
            $this->createOrUpdateProduct($product);
        }
    }

    /**
     * @param array $product
     */
    private function createOrUpdateProduct(array $product): void
    {
        $this->doctrineWriter->writeItem([
            'code' => $product['Product Code'],
            'name' => $product['Product Name'],
            'description' => $product['Product Description'],
            'price' => $product['Cost in GBP'],
            'stock' => $product['Stock'],
            'discontinued' => 'yes' == $product['Discontinued'] ? true : false,
        ]);
    }

    /**
     * @return array
     */
    public function getResults(): array
    {
        return [
            ['title' => 'total', 'value' => $this->affected + $this->effected + $this->updated],
            ['title' => 'skipped', 'value' => $this->affected],
            ['title' => 'imported', 'value' => $this->effected],
            ['title' => 'updated', 'value' => $this->updated],
        ];
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }
}
