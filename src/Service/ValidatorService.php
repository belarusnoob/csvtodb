<?php

declare(strict_types=1);

namespace App\Service;

class ValidatorService
{
    const MIN_PRICE = 5;
    const MAX_PRICE = 1000;
    const MIN_QUANTITY = 10;

    /**
     * @var array
     */
    private $product;

    /**
     * @var array
     */
    private $errors;

    /**
     * @var string
     */
    private $format;

    /**
     * @var bool
     */
    private $isValid = true;

    /**
     * @var array
     */
    private $methodsList = [
        'validateName' => 'invalid Name',
        'validateCode' => 'invalid Code',
        'validateDescription' => 'invalid Description',
        'validatePrice' => 'invalid Price',
        'validateStock' => 'invalid Stock',
        'validateMinPriceAndStock' => 'Price and Stock less then min',
        'validateMaxPrice' => 'Price more then max',
    ];

    /**
     * ProductValidator constructor.
     *
     * @param array $product
     */
    public function __construct(array $product)
    {
        $this->product = $product;
        $this->format = $this->product['Product Code'] . ' has %s';
    }

    /**
     * @return bool
     */
    public function validateName(): bool
    {
        return !empty($this->product['Product Name']);
    }

    /**
     * @return bool
     */
    public function validateCode(): bool
    {
        return !empty($this->product['Product Code']);
    }

    /**
     * @return bool
     */
    public function validateDescription(): bool
    {
        return !empty($this->product['Product Description']);
    }

    /**
     * @return bool
     */
    public function validatePrice(): bool
    {
        return
            !empty($this->product['Cost in GBP']) &&
            is_numeric((int) $this->product['Cost in GBP']);
    }

    /**
     * @return bool
     */
    public function validateStock(): bool
    {
        return
            !empty($this->product['Stock']) &&
            is_numeric((int) $this->product['Stock']);
    }

    /**
     * @return bool
     */
    public function validateMinPriceAndStock(): bool
    {
        return
            !empty($this->product['Cost in GBP']) &&
            !empty($this->product['Stock']) &&
            self::MIN_PRICE > $this->product['Cost in GBP'] &&
            self::MIN_QUANTITY <= $this->product['Stock'] ? false : true;
    }

    /**
     * @return bool
     */
    public function validateMaxPrice(): bool
    {
        return
            !empty($this->product['Cost in GBP']) &&
            self::MAX_PRICE > $this->product['Cost in GBP'];
    }

    /**
     * @return bool
     */
    public function isValid(): bool
    {
        foreach ($this->methodsList as $method => $message) {
            if (!$result = $this->$method()) {
                $this->isValid = $this->isValid && $result;
                $this->errors[] = sprintf($this->format, $message);
            }
        }

        return $this->isValid;
    }

    /**
     * @return array|null
     */
    public function getErrors(): ?array
    {
        return $this->errors;
    }
}
